#ifndef OBJECT_H
#define OBJECT_H

/* cwp.h - Header file for CWP (C With Prototypes)
 * Copyright (C) 2019  Alessio Vanni
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdarg.h>

/* Error codes returned by CWP functions */
typedef enum {
     cwpNONE, /* No errors occurred */
     cwpARG, /* One of the arguments is invalid */
     cwpNOMEM, /* The system has no more available memory */
} cwp_error_t;

/* Every object is of this type */
typedef struct object *cwp_object_t;

/* Every method has this signature */
typedef void (*cwp_method_t)(cwp_error_t *,
			     const cwp_object_t,
			     void *,
			     va_list *);

/* Always-existing prototypes */
extern const cwp_object_t cwpObject;
extern const cwp_object_t cwpEmpty;
extern const cwp_object_t cwpString;
extern const cwp_object_t cwpInt;
extern const cwp_object_t cwpNat;
extern const cwp_object_t cwpLongInt;
extern const cwp_object_t cwpLongNat;
extern const cwp_object_t cwpLongLongInt;
extern const cwp_object_t cwpLongLongNat;
extern const cwp_object_t cwpFloat;
extern const cwp_object_t cwpDouble;

/* Assign a value to an object's property.
 * If the object doesn't have the property, it will be created,
 * otherwise the value will be changed.
 * This function doesn't return any value.
 * ERROR is a pointer to a cwp_error_t variable. In case of success,
 * the variable will be set to `cwpNONE', to a different value otherwise.
 * SELF is the object whose property the value will be assigned to.
 * NAME is a string whose content is the same as the property's name.
 * VALUE is the value that will be assigned to the property.
 */
void cwp_set(cwp_error_t *error,
	     const cwp_object_t self,
	     const char *name,
	     cwp_object_t value);

/* Return the value assigned to an object's property.
 * If the object doesn't have the property, the returned value is `cwpEmpty'.
 * ERROR is a pointer to a cwp_error_t variable. In case of success,
 * the variable will be set to `cwpNONE', to a different value otherwise.
 * SELF is the object whose property's assigned value is returned.
 * NAME is a string whose content is the same as the property's name.
 * RETURNED is a pointer to a cwp_obect_t variable. At the end of the function's
 * execution, the variable will contain the value assigned to the property.
 */
void cwp_get(cwp_error_t *error,
	     const cwp_object_t self,
	     const char *name,
	     cwp_object_t *returned);

/* Permanently remove a property from an object.
 * If the object doesn't have the property, nothing happens,
 * otherwise, the object will not have that property anymore.
 * ERROR is a pointer to a cwp_error_t variable. In case of success,
 * the variable will be set to `cwpNONE', to a different value otherwise.
 * SELF is the object whose property will be removed.
 * NAME is a string whose content is the same as the property's name.
 */
void cwp_unset(cwp_error_t *error,
	       const cwp_object_t self,
	       const char *name);

/* Assign a function to an object's method.
 * If the object doesn't have the method, it will be created,
 * otherwise the value will be changed.
 * This function doesn't return any value.
 * ERROR is a pointer to a cwp_error_t variable. In case of success,
 * the variable will be set to `cwpNONE', to a different value otherwise.
 * SELF is the object whose method the value will be assigned to.
 * NAME is a string whose content is the same as the method's name.
 * VALUE is the function that will be assigned to the method.
 */
void cwp_method(cwp_error_t *error,
		const cwp_object_t self,
		const char *name,
		cwp_method_t function);

/* Execute an object's method.
 * If the object doesn't have the method (or the method is NULL),
 * nothing will happen.
 * The returned value is the method's returned value, or `cwpEmpty'
 * in case of errors.
 * ERROR is a pointer to a cwp_error_t variable. In case of success,
 * the variable will be set to `cwpNONE', to a different value otherwise.
 * SELF is the object whose property the value will be assigned to.
 * NAME is a string whose content is the same as the method's name.
 * RETURNED is a pointer to a variable of the type expected by the method.
 * If the type is different, it can generate subtle errors or corrupt memory
 * (mostly when the size of the type is different.)
 * The other arguments depend on the called method.
 */
void cwp_call(cwp_error_t *error,
	      const cwp_object_t self,
	      const char *name,
	      void *returned,
	      ...);

/* Permanently remove a method from an object.
 * If the object doesn't have the method, nothing happens,
 * otherwise, the object will not have that method anymore.
 * ERROR is a pointer to a cwp_error_t variable. In case of success,
 * the variable will be set to `cwpNONE', to a different value otherwise.
 * SELF is the object whose method will be removed.
 * NAME is a string whose content is the same as the method's name.
 */
void cwp_unmethod(cwp_error_t *error,
		  const cwp_object_t self,
		  const char *name);

/* Print error codes in a human readable way.
 * ERROR is the error code to print.
 * TAG is a string which will be prepended to the error code.
 * If NULL, it will be omitted.
 * OUT is where to print the error code to (e.g. `stderr'.)
 */
void cwp_log_error(cwp_error_t error, const char *tag, FILE *out);

/* Assign an error code to the ERROR argument.
 * CODE is the code to assign.
 * This macro makes sense only inside methods or
 * functions with a similar signature.
 */
#define cwp_error(code)				\
     do {					\
	  if (error) {*error = code;}		\
     } while (0)

/* Assign a value to the RETURNED argument.
 * VALUE is the value to assign.
 * TYPE is the type of the value to return.
 * This macro makes sense only inside methods or
 * functions with a similar signature.
 */ 
#define cwp_return(value, type)				\
     do {						\
	  if (returned) {*(type *)returned = value;}	\
     } while(0)

/* Get one of the optional arguments.
 * TYPE is the type of the argument.
 * This macro makes sense only inside methods or
 * functions with a similar signature.
 */ 
#define cwp_argument(type) va_arg(*args, type)

/* Define a method.
 * This macro will expand to a function signature.
 * NAME is the method's name.
 * This macro can't appear inside other functions and
 * must be followed immediately by a semicolon or by an open curly brace.
 */ 
#define cwp_defmethod(name)					\
     void name(cwp_error_t *error, const cwp_object_t self,	\
	       void *returned, va_list *args)

/* Define a prototype constructor.
 * This macro will expand to a function signature.
 * NAME is the constructor's name.
 * The optional arguments will be copied as-is during expansion,
 * thus must be valid argument declarations (e.g. `int x').
 * This macro can't appear inside other functions and
 * must be followed immediately by a semicolon or by an open curly brace.
 * Inside the constructor, use `cwp_error' and `cwp_return' to set the
 * error code and the returned value respectively.
 */ 
#define cwp_defclass(name, ...)						\
     void name(cwp_error_t *error, cwp_object_t *returned, ##__VA_ARGS__)

#endif
