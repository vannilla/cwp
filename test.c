/* test.c - A simple test case for CWP (C With Prototypes)
 * Copyright (C) 2019  Alessio Vanni
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include "cwp.h"

/* This file contains a simple test of the CWP implementation.
 * This test is NOT exhaustive.
 * Please don't use this file to check for bugs, only to see how
 * CWP works.
 */

static cwp_defclass(Point, int x, int y) {
     cwp_object_t p;
     cwp_object_t ox, oy;
     
     cwp_call(error, cwpObject, "create", &p);
     if (*error != cwpNONE) {
	  cwp_return(cwpEmpty, cwp_object_t);

	  return;
     }

     cwp_call(error, cwpInt, "create", &ox, x);
     if (*error != cwpNONE) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  cwp_call(NULL, p, "destroy", NULL);

	  return;
     }

     cwp_call(error, cwpInt, "create", &oy, y);
     if (*error != cwpNONE) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  cwp_call(NULL, p, "destroy", NULL);
	  cwp_call(NULL, ox, "destroy", NULL);

	  return;
     }

     cwp_set(error, p, "x", ox);
     if (*error != cwpNONE) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  cwp_call(NULL, p, "destroy", NULL);
	  cwp_call(NULL, ox, "destroy", NULL);
	  cwp_call(NULL, oy, "destroy", NULL);

	  return;
     }

     cwp_set(error, p, "y", oy);
     if (*error != cwpNONE) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  cwp_call(NULL, p, "destroy", NULL);
	  cwp_call(NULL, ox, "destroy", NULL);
	  cwp_call(NULL, oy, "destroy", NULL);

	  return;
     }

     cwp_return(p, cwp_object_t);
}

int main(void) {
     cwp_object_t o;
     cwp_object_t s;
     cwp_object_t i;
     cwp_object_t t;
     cwp_error_t error;
     char *p;
     int a = 0;

     cwp_call(&error, cwpObject, "create", &o);
     cwp_log_error(error, "Object create", stdout);

     cwp_call(&error, cwpString, "create", &s, "Ciao!");
     cwp_log_error(error, "String create", stdout);

     cwp_call(&error, cwpInt, "create", &i, 3);
     cwp_log_error(error, "Integer create", stdout);
     cwp_call(&error, i, "value", &a);
     cwp_log_error(error, "Integer value", stdout);
     printf("%d\n", a);
     cwp_call(&error, i, "destroy", NULL);
     cwp_log_error(error, "Integer destroy", stdout);

     cwp_call(&error, o, "to string", &t);
     cwp_log_error(error, "Object to string", stdout);
     cwp_call(&error, t, "to c array", &p);
     printf("%s\n", p);
     free(p);
     cwp_call(&error, t, "destroy", NULL);
     cwp_log_error(error, "String destroy", stdout);

     cwp_call(&error, s, "to c array", &p);
     cwp_log_error(error, "String to c array", stdout);
     printf("%s\n", p);
     free(p);
     cwp_call(&error, s, "destroy", NULL);
     cwp_log_error(error, "String destroy", stdout);
     cwp_call(&error, o, "destroy", NULL);
     cwp_log_error(error, "Object destroy", stdout);

     cwp_call(&error, cwpObject, "create", &o);
     cwp_call(&error, cwpString, "create", &s, "test");
     cwp_set(&error, o, "test", s);
     cwp_get(&error, o, "test", &t);
     cwp_call(&error, t, "to c array", &p);
     printf("%s\n", p);
     free(p);
     cwp_call(&error, s, "destroy", NULL);
     cwp_get(&error, o, "test", &t);
     cwp_call(&error, t, "to c array", &p);
     printf("%s\n", p);
     free(p);
     cwp_call(&error, o, "destroy", NULL);

     Point(&error, &o, 10, 33);
     cwp_log_error(error, "Point", stdout);
     cwp_get(&error, o, "x", &s);
     cwp_call(&error, s, "value", &a);
     printf("%d\n", a);
     cwp_get(&error, o, "y", &s);
     cwp_call(&error, s, "value", &a);
     printf("%d\n", a);
     cwp_call(&error, o, "destroy", NULL);
     
     return 0;
}
