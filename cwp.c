/* cwp.c - Implementation file for CWP (C With Prototypes)
 * Copyright (C) 2019  Alessio Vanni
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <search.h>

#include "cwp.h"

/* Properties have this structure */
struct property {
     const char *name;
     cwp_object_t value;
};

/* Methods have this structure */
struct method {
     const char *name;
     cwp_method_t value; /* Function pointer */
};

/* Object can contain these primitive types */
struct primitive {
     const char *name;
     char *string_value;
     int int_value;
     unsigned int uint_value;
     long int long_value;
     unsigned long int ulong_value;
     long long int longlong_value;
     unsigned long long int ulonglong_value;
     float float_value;
     double double_value;
};

/* Objects have this structure */
struct object {
     size_t nref; /* How many object contain this object */
     size_t nprops; /* Number of properties */
     struct property *properties; /* Vector of properties */
     size_t nmeths; /* Number of methods */
     struct method *methods; /* Vector of methods */
     size_t nprivs; /* Number of primitive type values */
     struct primitive *primitives; /* Vector of primitive type values */
};

/* Forward declarations: */

/* cwpObject's methods */
static cwp_defmethod(object_create);
static cwp_defmethod(object_destroy);
static cwp_defmethod(object_equals);
static cwp_defmethod(object_tostring);

/* cwpEmpty's methods */
static cwp_defmethod(empty_tostring);

/* cwpString's methods */
static cwp_defmethod(string_create);
static cwp_defmethod(string_destroy);
static cwp_defmethod(string_equals);
static cwp_defmethod(string_length);
static cwp_defmethod(string_toarray);

/* Create boxed numbers */
static cwp_defmethod(integer_create);
static cwp_defmethod(natural_create);
static cwp_defmethod(longinteger_create);
static cwp_defmethod(longnatural_create);
static cwp_defmethod(llonginteger_create);
static cwp_defmethod(llongnatural_create);
static cwp_defmethod(float_create);
static cwp_defmethod(double_create);

/* Return the boxed number */
static cwp_defmethod(integer_value);
static cwp_defmethod(natural_value);
static cwp_defmethod(longinteger_value);
static cwp_defmethod(longnatural_value);
static cwp_defmethod(llonginteger_value);
static cwp_defmethod(llongnatural_value);
static cwp_defmethod(float_value);
static cwp_defmethod(double_value);

/* Boxed number equality */
static cwp_defmethod(number_equals);

/* cwpEmpty's definition */
static struct method empty_methods[] = {
     {"to string", empty_tostring},
     {"as string", empty_tostring},
     {"equals", object_equals},
};

static struct object empty = {
     1,
     0, NULL,
     sizeof(empty_methods)/sizeof(empty_methods[0]), empty_methods,
     0, NULL,
};

/* cwpObject's definition */
static struct property object_properties[] = {
     {"super", &empty},
};

static struct method object_methods[] = {
     {"create", object_create},
     {"destroy", NULL},
     {"to string", object_tostring},
     {"as string", object_tostring},
     {"equals", object_equals},
};

static struct object object = {
     1,
     sizeof(object_properties)/sizeof(object_properties[0]), object_properties,
     sizeof(object_methods)/sizeof(object_methods[0]), object_methods,
     0, NULL,
};

/* Boxed values' definition */
static struct property box_properties[] = {
     {"super", &object},
};

/* cwpString's definition */
static struct method string_methods[] = {
     {"create", string_create},
     {"destroy", NULL},
     {"equals", string_equals},
     {"length", string_length},
     {"size", string_length},
     {"as c array", string_toarray},
     {"to c array", string_toarray},
};

static struct primitive string_primitives[] = {
     {"length", NULL, 0, 0, 0, 0, 0, 0, 0, 0},
     {"size", NULL, 0, 0, 0, 0, 0, 0, 0, 0},
     {"value", "", 0, 0, 0, 0, 0, 0, 0, 0},
};

static struct object string = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(string_methods)/sizeof(string_methods[0]), string_methods,
     sizeof(string_primitives)/sizeof(string_primitives[0]), string_primitives,
};

/* Boxed numbers' definition */
static struct primitive number_primitives[] = {
     {"value", NULL, 0, 0, 0, 0, 0, 0, 0, 0},
};

/* cwpInt's definition */
static struct method integer_methods[] = {
     {"create", integer_create},
     {"destroy", NULL},
     {"equals", number_equals},
     {"value", integer_value},
};

static struct object integer = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(integer_methods)/sizeof(integer_methods[0]), integer_methods,
     sizeof(number_primitives)/sizeof(number_primitives[0]), number_primitives,
};

/* cwpNat's definition */
static struct method natural_methods[] = {
     {"create", natural_create},
     {"destroy", NULL},
     {"equals", number_equals},
     {"value", natural_value},
};

static struct object natural = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(natural_methods)/sizeof(natural_methods[0]), natural_methods,
     sizeof(number_primitives)/sizeof(number_primitives[0]), number_primitives,
};

/* cwpLongInt's definition */
static struct method longinteger_methods[] = {
     {"create", longinteger_create},
     {"destroy", NULL},
     {"equals", number_equals},
     {"value", longinteger_value},
};

static struct object longinteger = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(longinteger_methods)/sizeof(longinteger_methods[0]), longinteger_methods,
     sizeof(number_primitives)/sizeof(number_primitives[0]), number_primitives,
};

/* cwpLongNat's definition */
static struct method longnatural_methods[] = {
     {"create", longnatural_create},
     {"destroy", NULL},
     {"equals", number_equals},
     {"value", longnatural_value},
};

static struct object longnatural = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(longnatural_methods)/sizeof(longnatural_methods[0]), longnatural_methods,
     sizeof(number_primitives)/sizeof(number_primitives[0]), number_primitives,
};

/* cwpLongLongInt's definition */
static struct method llonginteger_methods[] = {
     {"create", llonginteger_create},
     {"destroy", NULL},
     {"equals", number_equals},
     {"value", llonginteger_value},
};

static struct object llonginteger = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(llonginteger_methods)/sizeof(llonginteger_methods[0]), llonginteger_methods,
     sizeof(number_primitives)/sizeof(number_primitives[0]), number_primitives,
};

/* cwpLongLongNat's definition */
static struct method llongnatural_methods[] = {
     {"create", llongnatural_create},
     {"destroy", NULL},
     {"equals", number_equals},
     {"value", llongnatural_value},
};

static struct object llongnatural = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(llongnatural_methods)/sizeof(llongnatural_methods[0]), llongnatural_methods,
     sizeof(number_primitives)/sizeof(number_primitives[0]), number_primitives,
};

/* cwpFloat's definition */
static struct method float_methods[] = {
     {"create", float_create},
     {"destroy", NULL},
     {"equals", number_equals},
     {"value", float_value},
};

static struct object floatn = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(float_methods)/sizeof(float_methods[0]), float_methods,
     sizeof(number_primitives)/sizeof(number_primitives[0]), number_primitives,
};

/* cwpDouble's definition */
static struct method double_methods[] = {
     {"create", double_create},
     {"destroy", NULL},
     {"equals", number_equals},
     {"value", double_value},
};

static struct object doublen = {
     1,
     sizeof(box_properties)/sizeof(box_properties[0]), box_properties,
     sizeof(double_methods)/sizeof(double_methods[0]), double_methods,
     sizeof(number_primitives)/sizeof(number_primitives[0]), number_primitives,
};

/* Assign the definitions to the prototypes */
const cwp_object_t cwpObject = &object;
const cwp_object_t cwpEmpty = &empty;
const cwp_object_t cwpString = &string;
const cwp_object_t cwpInt = &integer;
const cwp_object_t cwpNat = &natural;
const cwp_object_t cwpLongInt = &longinteger;
const cwp_object_t cwpLongNat = &longnatural;
const cwp_object_t cwpLongLongInt = &llonginteger;
const cwp_object_t cwpLongLongNat = &llongnatural;
const cwp_object_t cwpFloat = &floatn;
const cwp_object_t cwpDouble = &doublen;

static int propcmp(const void *a, const void *b) {
     /* Compare two properties A and B. */
     
     const struct property *p1 = (const struct property *)a;
     const struct property *p2 = (const struct property *)b;

     return strcmp(p1->name, p2->name);
}

static int methodcmp(const void *a, const void *b) {
     /* Compare two methods A and B. */
     
     const struct method *m1 = (const struct method *)a;
     const struct method *m2 = (const struct method *)b;

     return strcmp(m1->name, m2->name);
}

void cwp_set(cwp_error_t *error, const cwp_object_t self,
	     const char *name, cwp_object_t value) {
     /* Assign a value to an object's property.
      * If the object doesn't have the property, it will be created,
      * otherwise the value will be changed.
      * This function doesn't return any value.
      * ERROR is a pointer to a cwp_error_t variable. In case of success,
      * the variable will be set to `cwpNONE', to a different value otherwise.
      * SELF is the object whose property the value will be assigned to.
      * NAME is a string whose content is the same as the property's name.
      * VALUE is the value that will be assigned to the property.
      */
     
     struct property *p;
     struct property tmp;
     size_t size;
     char *copy;
     cwp_error_t terr; /* If `error' is NULL */

     /* Check arguments */
     if (self == NULL || name == NULL || value == NULL) {
	  cwp_error(cwpARG);
	  
	  return;
     }
     
     cwp_error(cwpNONE);

     /* Can't assign values to these prototypes */
     if (self == cwpObject || self == cwpEmpty
	 || self == cwpString || self == cwpInt
	 || self == cwpNat || self == cwpLongInt
	 || self == cwpLongNat || self == cwpLongLongInt
	 || self == cwpLongLongNat || self == cwpFloat
	 || self == cwpDouble) {
	  return;
     }

     /* Check if the property exists */
     size = self->nprops;
     tmp.name = name;
     tmp.value = cwpEmpty;

     p = lfind(&tmp, self->properties, &size, sizeof(struct property), propcmp);

     /* Increase the number of references unless we are setting ourselves */
     if (value != self) {
	  value->nref += 1;
     }

     if (p != NULL) {
	  /* The property was found */
	  if (p->value != cwpObject && p->value != cwpEmpty && p->value != self) {
	       /* Decrease the old value's number of references and
		  destroy it if needed */
	       p->value->nref -= 1;
	       if (p->value->nref <= 1) {
		    cwp_call(&terr, p->value, "destroy", NULL);

		    /* On error don't assign the new value since we
		       couldn't destroy the old value. */
		    if (terr != cwpNONE) {
			 cwp_error(terr);
			 return;
		    }
	       }
	  }

	  p->value = value;
     } else {
	  /* The property was not found, create it */
	  if (asprintf(&copy, "%s", name) < 0) {
	       cwp_error(cwpNOMEM);
	       value->nref -= 1;

	       return;
	  }
	  
	  self->nprops += 1;
	  p = reallocarray(self->properties, self->nprops, sizeof(struct property));
	  if (p == NULL) {
	       cwp_error(cwpNOMEM);
	       
	       return;
	  }

	  self->properties = p;
	  self->properties[self->nprops-1].name = copy;
	  self->properties[self->nprops-1].value = value;
     }
}

void cwp_get(cwp_error_t *error, const cwp_object_t self,
	     const char *name, cwp_object_t *returned) {
     /* Return the value assigned to an object's property.
      * If the object doesn't have the property, the returned value is `cwpEmpty'.
      * ERROR is a pointer to a cwp_error_t variable. In case of success,
      * the variable will be set to `cwpNONE', to a different value otherwise.
      * SELF is the object whose property's assigned value is returned.
      * NAME is a string whose content is the same as the property's name.
      * RETURNED is a pointer to a cwp_obect_t variable. At the end of the function's
      * execution, the variable will contain the value assigned to the property.
      */
     
     struct property *p;
     struct property tmp;
     size_t size;

     /* Check arguments */
     if (self == NULL || name == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(cwpEmpty, cwp_object_t);

	  return;
     }
     
     cwp_error(cwpNONE);

     /* Naturally no properties means nothing worthwile to return */
     if (self->nprops == 0) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  
	  return;
     }

     /* Check if the property exists */
     size = self->nprops;
     tmp.name = name;
     tmp.value = cwpEmpty;

     p = lfind(&tmp, self->properties, &size, sizeof(struct property), propcmp);

     if (p == NULL) {
	  /* If the property doesn't exists it's the same as 
	     having `cwpEmpty'. */
	  cwp_return(cwpEmpty, cwp_object_t);

	  return;
     }

     cwp_return(p->value, cwp_object_t);
}

void cwp_unset(cwp_error_t *error, const cwp_object_t self, const char *name) {
     /* Permanently remove a property from an object.
      * If the object doesn't have the property, nothing happens,
      * otherwise, the object will not have that property anymore.
      * ERROR is a pointer to a cwp_error_t variable. In case of success,
      * the variable will be set to `cwpNONE', to a different value otherwise.
      * SELF is the object whose property will be removed.
      * NAME is a string whose content is the same as the property's name.
      */
     
     struct property *p;
     struct property tmp;
     size_t i;

     /* Check arguments */
     if (self == NULL || name == NULL) {
	  cwp_error(cwpARG);

	  return;
     }
     
     cwp_error(cwpNONE);

     /* Can't remove properties of these prototypes */
     if (self == cwpObject || self == cwpEmpty
	 || self == cwpString || self == cwpInt
	 || self == cwpNat || self == cwpLongInt
	 || self == cwpLongNat || self == cwpLongLongInt
	 || self == cwpLongLongNat || self == cwpFloat
	 || self == cwpDouble) {
	  return;
     }

     /* No properties means nothing to do */
     if (self->nprops == 0) {
	  return;
     }

     /* We need the property's index */
     for (i=1; i<self->nprops; ++i) {
	  if (strcmp(self->properties[i].name, name) == 0) {
	       break;
	  }
     }

     /* These properties can't be removed */
     if (i < object.nprops) {
	  return;
     }

     /* The property was not found */
     if (i == self->nprops) {
	  return;
     }

     /* Swap the property's position with the property at the end 
	of the vector */
     tmp.name = self->properties[i].name;
     tmp.value = self->properties[i].value;

     self->properties[i].name = self->properties[self->nprops-1].name;
     self->properties[i].value = self->properties[self->nprops-1].value;

     /* Change the vector's size */
     self->nprops -= 1;
     p = reallocarray(self->properties, self->nprops, sizeof(struct property));
     if (p == NULL) {
	  cwp_error(cwpNOMEM);

	  /* Undo the last change */
	  self->nprops += 1;
	  self->properties[i].name = tmp.name;
	  self->properties[i].value = tmp.value;

	  return;
     }

     /* Decrease the number of references of the deleted property's value
	and destroy it if needed */
     if (tmp.value != self) {
	  tmp.value->nref -= 1;
	  if (tmp.value->nref <= 1) {
	       cwp_call(NULL, tmp.value, "destroy", NULL);
	       free((char *)tmp.name);
	  }
     }
}

void cwp_method(cwp_error_t *error, const cwp_object_t self,
		const char *name, cwp_method_t function) {
     /* Assign a function to an object's method.
      * If the object doesn't have the method, it will be created,
      * otherwise the value will be changed.
      * This function doesn't return any value.
      * ERROR is a pointer to a cwp_error_t variable. In case of success,
      * the variable will be set to `cwpNONE', to a different value otherwise.
      * SELF is the object whose method the value will be assigned to.
      * NAME is a string whose content is the same as the method's name.
      * VALUE is the function that will be assigned to the method.
      */
     
     struct method *m;
     struct method tmp;
     size_t size;
     char *copy;

     /* Check arguments */
     if (self == NULL || name == NULL || function == NULL) {
	  cwp_error(cwpARG);

	  return;
     }

     cwp_error(cwpNONE);

     /* Can't assign to these prototypes */
     if (self == cwpObject || self == cwpEmpty
	 || self == cwpString || self == cwpInt
	 || self == cwpNat || self == cwpLongInt
	 || self == cwpLongNat || self == cwpLongLongInt
	 || self == cwpLongLongNat || self == cwpFloat
	 || self == cwpDouble) {
	  return;
     }

     /* Check if the method exists */
     size = self->nmeths;
     tmp.name = name;
     tmp.value = function;

     m = lfind(&tmp, self->methods, &size, sizeof(struct method), methodcmp);

     if (m != NULL) {
	  /* The method was found, just change the value */
	  m->value = function;
     } else {
	  /* The method was not found, create it */
	  if (asprintf(&copy, "%s", name) < 0) {
	       cwp_error(cwpNOMEM);

	       return;
	  }
	  
	  self->nmeths += 1;
	  m = reallocarray(self->methods, self->nmeths, sizeof(struct method));
	  if (m == NULL) {
	       cwp_error(cwpNOMEM);

	       return;
	  }

	  self->methods = m;
	  self->methods[self->nmeths-1].name = copy;
	  self->methods[self->nmeths-1].value = function;
     }
}

void cwp_call(cwp_error_t *error, const cwp_object_t self,
	      const char *name, void *returned, ...) {
     /* Execute an object's method.
      * If the object doesn't have the method (or the method is NULL),
      * nothing will happen.
      * The returned value is the method's returned value, or `cwpEmpty'
      * in case of errors.
      * ERROR is a pointer to a cwp_error_t variable. In case of success,
      * the variable will be set to `cwpNONE', to a different value otherwise.
      * SELF is the object whose property the value will be assigned to.
      * NAME is a string whose content is the same as the method's name.
      * RETURNED is a pointer to a variable of the type expected by the method.
      * If the type is different, it can generate subtle errors or corrupt memory
      * (mostly when the size of the type is different.)
      * The other arguments depend on the called method.
      */
     
     struct method *m;
     struct method tmp;
     size_t size;
     va_list args;

     /* Check arguments */
     if (self == NULL || name == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(cwpEmpty, cwp_object_t);

	  return;
     }

     cwp_error(cwpNONE);

     /* No methods means nothing to call */
     if (self->nmeths == 0) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  
	  return;
     }

     /* Check if the method exists */
     size = self->nmeths;
     tmp.name = name;
     tmp.value = NULL;

     m = lfind(&tmp, self->methods, &size, sizeof(struct method), methodcmp);

     /* The method was not found, return an error */
     if (m == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     /* The method was found, but is NULL, so do nothing */
     if (m->value == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     /* Initialize the optional arguments and call the method */
     va_start(args, returned);

     m->value(error, self, returned, &args);
     
     va_end(args);
}

void cwp_unmethod(cwp_error_t *error, const cwp_object_t self, const char *name) {
     /* Permanently remove a method from an object.
      * If the object doesn't have the method, nothing happens,
      * otherwise, the object will not have that method anymore.
      * ERROR is a pointer to a cwp_error_t variable. In case of success,
      * the variable will be set to `cwpNONE', to a different value otherwise.
      * SELF is the object whose method will be removed.
      * NAME is a string whose content is the same as the method's name.
      */
     
     struct method *m;
     struct method tmp;
     size_t i;

     /* Check arguments */
     if (self == NULL || name == NULL) {
	  cwp_error(cwpARG);

	  return;
     }

     cwp_error(cwpNONE);

     /* Can't remove methods of these prototypes */
     if (self == cwpObject || self == cwpEmpty
	 || self == cwpString || self == cwpInt
	 || self == cwpNat || self == cwpLongInt
	 || self == cwpLongNat || self == cwpLongLongInt
	 || self == cwpLongLongNat || self == cwpFloat
	 || self == cwpDouble) {
	  return;
     }

     /* No methods means nothing to do */
     if (self->nmeths == 0) {
	  return;
     }

     /* We need the method's index */
     for (i=0; i<self->nmeths; ++i) {
	  if (strcmp(self->methods[i].name, name) == 0) {
	       break;
	  }
     }

     /* Can't remove these methods */
     if (i < object.nmeths) {
	  return;
     }

     /* The method was not found */
     if (i == self->nmeths) {
	  return;
     }

     /* Swap the property's position with the property at the end 
	of the vector */
     tmp.name = self->methods[i].name;
     tmp.value = self->methods[i].value;

     self->methods[i].name = self->methods[self->nmeths-1].name;
     self->methods[i].value = self->methods[self->nmeths-1].value;

     /* Change the vector's size */
     self->nmeths -= 1;
     m = reallocarray(self->methods, self->nmeths, sizeof(struct method));
     if (m == NULL) {
	  cwp_error(cwpNOMEM);

	  /* Undo the last change */
	  self->methods[i].name = tmp.name;
	  self->methods[i].value = tmp.value;
     }
}

void cwp_log_error(cwp_error_t error, const char *tag, FILE *out) {
     /* Print error codes in a human readable way.
      * ERROR is the error code to print.
      * TAG is a string which will be prepended to the error code.
      * If NULL, it will be omitted.
      * OUT is where to print the error code to (e.g. `stderr'.)
      */
     
     char *s;

     if (out == NULL) {
	  return;
     }

     switch (error) {
     case cwpNONE:
	  s = "no error occurred";
	  break;
     case cwpARG:
	  s = "an invalid argument was detected";
	  break;
     case cwpNOMEM:
	  s = "the system is out of memory";
	  break;
     }

     if (tag != NULL) {
	  fprintf(out, "%s -- CWP Error: %s\n", tag, s);
     } else {
	  fprintf(out, "CWP Error: %s\n", s);
     }
}

static cwp_object_t oalloc(cwp_error_t *error,
			   size_t nprops, struct property *props,
			   size_t nmeths, struct method *methods,
			   size_t nprivs, struct primitive *privs) {
     /* Allocate a new object.
      * Return the allocated object.
      * ERROR is a pointer to a cwp_error_t variable. In case of success,
      * the variable will be set to `cwpNONE', to a different value otherwise.
      * NPROPS is the number of properties to copy to the new object.
      * PROPS is the vector of properties to copy.
      * NMETHS is the number of methods to copy to the new object.
      * METHODS is the vector of methods to copy.
      * NPRIVS is the number of primitive values to copy to the new object.
      * PRIVS is the vector of primitive values to copy.
      */ 
     
     cwp_object_t r;
     size_t i;
     char *c;

     r = calloc(1, sizeof(struct object));
     if (r == NULL) {
	  cwp_error(cwpNOMEM);

	  return NULL;
     }

     r->nref = 1;
     r->nprops = nprops;
     r->nmeths = nmeths;
     r->nprivs = nprivs;

     if (r->nprops > 0) {
	  r->properties = calloc(r->nprops, sizeof(struct property));
	  if (r->properties == NULL) {
	       free(r);
	       cwp_error(cwpNOMEM);

	       return NULL;
	  }
     }

     if (r->nmeths > 0) {
	  r->methods = calloc(r->nmeths, sizeof(struct method));
	  if (r->methods == NULL) {
	       free(r->properties);
	       free(r);
	       cwp_error(cwpNOMEM);

	       return NULL;
	  }
     }

     if (r->nprops > 0) {
	  r->properties = memcpy(r->properties, props,
				 r->nprops*sizeof(struct property));
     }

     if (r->nmeths > 0) {
	  r->methods = memcpy(r->methods, methods,
			      r->nmeths*sizeof(struct method));
     }

     if (r->nprivs > 0) {
	  r->primitives = calloc(r->nprivs, sizeof(struct primitive));
	  if (r->primitives == NULL) {
	       free(r->properties);
	       free(r->methods);
	       cwp_error(cwpNOMEM);

	       return NULL;
	  }
	  
	  r->primitives = memcpy(r->primitives, privs,
				 r->nprivs*sizeof(struct primitive));
     }

     /* Duplicate the property/method name to avoid data corruption */
     for (i=0; i<r->nprops; ++i) {
	  if (i > object.nprops) {
	       c = strdup(r->properties[i].name);
	       r->properties[i].name = (c != NULL) ? c : r->properties[i].name;
	  }
	  r->properties[i].value->nref += 1;
     }

     for (i=object.nmeths; i<r->nmeths; ++i) {
	  c = strdup(r->methods[i].name);
	  r->methods[i].name = (c != NULL) ? c : r->methods[i].name;
     }

     return r;
}

static cwp_defmethod(object_create) {
     /* Method to create a new object.
      * Return the created object as a cwp_object_t or `cwpEmpty' on error.
      * Doesn't require additional arguments.
      */
     
     cwp_object_t r;
     
     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(cwpEmpty, cwp_object_t);

	  return;
     }

     r = oalloc(error,
		object.nprops, object.properties,
		object.nmeths, object.methods,
		0, NULL);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(object_destroy) {
     /* Method to destroy an object.
      * Return `cwpEmpty'.
      * Doesn't require additional arguments.
      */
     size_t i;
     cwp_error_t terr; /* If `error' is NULL */

     cwp_error(cwpNONE);
     cwp_return(cwpEmpty, cwp_object_t);

     if (self == NULL) {
	  cwp_error(cwpARG);

	  return;
     }

     if (self->nref <= 1) {
	  /* Decrease the number of references for each stored object,
	     then destroy it if needed */
	  for (i=0; i<self->nprops; ++i) {
	       /* Don't call "destroy" on ourselves */
	       if (self->properties[i].value != self) {
		    self->properties[i].value->nref -= 1;
		    cwp_call(&terr, self->properties[i].value, "destroy", NULL);
		    
		    if (terr != cwpNONE) {
			 cwp_error(terr);
			 return;
		    }
	       }

	       if (i >= object.nprops) {
		    free((char *)self->properties[i].name);
	       }
	  }

	  for (i=object.nmeths; i<self->nmeths; ++i) {
	       free((char *)self->methods[i].name);
	  }

	  free(self->properties);
	  free(self->methods);
	  free(self);
     }
}

static cwp_defmethod(object_equals) {
     /* Method to compare two objects
      * Return 0 or 1 as an int.
      * 0 means the object are different, 1 means they are equal.
      * Requires the object to compare to as an additional argument.
      */
     
     cwp_object_t b;
     size_t i;
     int r;
     cwp_error_t terr; /* If `error' is NULL */

     /* Get the other object */
     b = cwp_argument(cwp_object_t);

     if (self == NULL || b == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, int);

	  return;
     }

     cwp_error(cwpNONE);

     /* If the pointer is the same, they are the same object */
     if (self == b) {
	  cwp_return(1, int);

	  return;
     }

     /* Comparison between prototypes */
     if ((self == cwpObject && b != cwpObject)
	 ||
	 (self == cwpEmpty && b != cwpEmpty)) {
	  cwp_return(0, int);
     }

     /* If the number of properties or methods is different,
	they must be different objects */
     if (self->nprops != b->nprops || self->nmeths != b->nmeths) {
	  cwp_return(0, int);

	  return;
     }

     for (i=0; i<self->nprops; ++i) {
	  /* If the names differs, they are different objects */
	  if (strcmp(self->properties[i].name, b->properties[i].name) != 0) {
	       cwp_return(0, int);

	       return;
	  }

	  /* Call "equals" on each property */
	  cwp_call(&terr, self->properties[i].value,
		   "equals", &r, b->properties[i].value);
	  if (terr != cwpNONE) {
	       cwp_error(terr);
	       cwp_return(0, int);

	       return;
	  }

	  /* If even one property says it's different, 
	     the objects are different */
	  if (r == 0) {
	       cwp_return(0, int);

	       return;
	  }
     }

     for (i=0; i<self->nmeths; ++i) {
	  /* If the names differ, they are different objects */
	  if (strcmp(self->properties[i].name, b->properties[i].name) != 0) {
	       cwp_return(0, int);

	       return;
	  }

	  /* If the pointers to function are different, 
	     they are different objects */
	  if (self->properties[i].value != b->properties[i].value) {
	       cwp_return(0, int);

	       return;
	  }
     }

     cwp_return(1, int);
}

static cwp_defmethod(object_tostring) {
     /* Method to transform the object into a string.
      * Return the string as a cwp_object_t.
      * In case of errors, return `cwpEmpty'.
      * Doesn't require additional arguments.
      */
     
     cwp_object_t string;
     cwp_error_t terr; /* If `error' is NULL */

     cwp_error(cwpNONE);

     /* An object in itself doesn't have much to say
	This method should be overridden */
     cwp_call(&terr, cwpString, "create", &string, "Object");
     if (terr != cwpNONE) {
	  cwp_error(terr);
	  cwp_return(cwpEmpty, cwp_object_t);

	  return;
     }

     cwp_return(string, cwp_object_t);
}

static cwp_defmethod(empty_tostring) {
     /* Method to transform the object into a string.
      * Return the string as a cwp_object_t.
      * In case of errors, return `cwpEmpty'.
      * Doesn't require additional arguments.
      */
     
     cwp_object_t string;
     cwp_error_t terr; /* If `error' is NULL */
     
     cwp_error(cwpNONE);

     /* `cwpEmtpy' has literally nothing to print anyway */
     cwp_call(&terr, cwpString, "create", &string, "Empty");
     if (terr != cwpNONE) {
	  cwp_error(terr);
	  cwp_return(cwpEmpty, cwp_object_t);

	  return;
     }

     cwp_return(string, cwp_object_t);
}

static cwp_defmethod(string_create) {
     /* Method to create a `cwpString' object.
      * Return the string as a cwp_object_t or `cwpEmpty' on error.
      * Require a C string (char *) as additional argument.
      * The C string will be used as the object's value.
      */
     
     cwp_object_t r;
     char *str, *copy;
     int size;
     
     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     /* Get the additional argument */
     str = cwp_argument(char *);
     if (str == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(cwpEmpty, cwp_object_t);
	  
	  return;
     }

     /* Copy the C string */
     size = asprintf(&copy, "%s", str);
     if (size < 0) {
	  cwp_error(cwpNOMEM);
	  cwp_return(cwpEmpty, cwp_object_t);
	  
	  return;
     }

     r = oalloc(error,
		string.nprops, string.properties,
		string.nmeths, string.methods,
		string.nprivs, string.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = string_destroy;

     /* Set the size and the value */
     r->primitives[0].int_value = size;
     r->primitives[1].int_value = size;
     r->primitives[2].string_value = copy;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(string_destroy) {
     /* Method to destroy a `cwpString' object.
      * Return `cwpEmpty'.
      * Doesn't require additional arguments.
      */
     
     cwp_error(cwpNONE);
     cwp_return(cwpEmpty, cwp_object_t);

     if (self == NULL) {
	  cwp_error(cwpARG);

	  return;
     }

     /* Destroy it only when there are no more references */
     if (self->nref > 1) {
	  return;
     }

     free(self->primitives[2].string_value);

     /* It's still a normal object */
     object_destroy(error, self, returned, args);
}

static cwp_defmethod(string_equals) {
     /* Method to compare two `cwpString' objects.
      * Return 0 or 1 as int;
      * Require a cwp_object_t as additional argument
      */
     
     cwp_object_t b;
     int r;

     /* Get the other object */
     b = cwp_argument(cwp_object_t);

     if (self == NULL || b == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, int);

	  return;
     }

     cwp_error(cwpNONE);

     /* If the other object isn't a `cwpString' object, the number of
	primitive values is always different */
     if (self->nprivs != b->nprivs) {
	  cwp_return(0, int);

	  return;
     }

     /* If the sizes differ, they are different strings */
     if (self->primitives[0].int_value != b->primitives[0].int_value) {
	  cwp_return(0, int);

	  return;
     }

     /* Actually compare the two strings */
     r = strcmp(self->primitives[2].string_value, b->primitives[2].string_value);

     cwp_return(!r, int);
}

static cwp_defmethod(string_length) {
     /* Method to get the lenght of a `cwpString' object.
      * Return the number of characters as an int.
      * Doesn't require additional arguments.
      */
     
     int size;
     
     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, int);

	  return;
     }
     
     cwp_error(cwpNONE);

     size = self->primitives[0].int_value;

     cwp_return(size, int);
}

static cwp_defmethod(string_toarray) {
     /* Method to get a C string (char *) from a `cwpString' object.
      * Return a pointer to the C string as a char *, NULL in case of errors.
      * The returned C string is always NUL-terminated.
      * The returned C string is always freshly allocated.
      * Always deallocate it with `free' when you're done with it.
      * Doesn't require additional characters.
      */
     
     char *copy;

     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(NULL, char *);

	  return;
     }

     /* Copy the object's string */
     if (asprintf(&copy, "%s", self->primitives[2].string_value) < 0) {
	  cwp_error(cwpNOMEM);
	  cwp_return(NULL, char *);

	  return;
     }
     
     cwp_error(cwpNONE);
     
     cwp_return(copy, char *);
}

static cwp_defmethod(integer_create) {
     /* Method to create a `cwpInt' object.
      * Return the new object as a cwp_object_t or `cwpEmpty' on error.
      * Require a number as an int as additional argument.
      */
     
     cwp_object_t r;
     int number;
     
     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     r = oalloc(error,
		integer.nprops, integer.properties,
		integer.nmeths, integer.methods,
		integer.nprivs, integer.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;

     number = cwp_argument(int);
     
     r->primitives[0].int_value = number;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(natural_create) {
     /* Method to create a `cwpNat' object.
      * Return the new object as a cwp_object_t, `cwpEmpty' on error.
      * Require a number as unsigned int as additional argument.
      */
     
     cwp_object_t r;
     unsigned int number;
     
     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     r = oalloc(error,
		natural.nprops, natural.properties,
		natural.nmeths, natural.methods,
		natural.nprivs, natural.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;

     number = cwp_argument(unsigned int);
     
     r->primitives[0].uint_value = number;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(longinteger_create) {
     /* Method to create a `cwpLongInt' object.
      * Return the new object as a cwp_object_t or `cwpEmpty' on error.
      * Require a number as long int as additional argument.
      */ 

     cwp_object_t r;
     long int number;

     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     r = oalloc(error,
		longinteger.nprops, longinteger.properties,
		longinteger.nmeths, longinteger.methods,
		longinteger.nprivs, longinteger.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;

     number = cwp_argument(long int);
     
     r->primitives[0].long_value = number;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(longnatural_create) {
     /* Method to create a `cwpLongNat' object.
      * Return the new object as a cwp_object_t or `cwpEmpty' on error.
      * Require a number as unsigned long int as additional argument.
      */ 

     cwp_object_t r;
     unsigned long int number;

     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     r = oalloc(error,
		longnatural.nprops, longnatural.properties,
		longnatural.nmeths, longnatural.methods,
		longnatural.nprivs, longnatural.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;

     number = cwp_argument(unsigned long int);
     
     r->primitives[0].ulong_value = number;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(llonginteger_create) {
     /* Method to create a `cwpLongLongInt' object.
      * Return the new object as a cwp_object_t or `cwpEmpty' on error.
      * Require a number as long long int as additional argument.
      */ 

     cwp_object_t r;
     long long int number;

     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     r = oalloc(error,
		llonginteger.nprops, llonginteger.properties,
		llonginteger.nmeths, llonginteger.methods,
		llonginteger.nprivs, llonginteger.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;

     number = cwp_argument(long long int);
     
     r->primitives[0].longlong_value = number;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(llongnatural_create) {
     /* Method to create a `cwpLongLongNat' object.
      * Return the new object as a cwp_object_t or `cwpEmpty' on error.
      * Require a number as unsigned long long int as additional argument.
      */ 

     cwp_object_t r;
     unsigned long long int number;

     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     r = oalloc(error,
		llongnatural.nprops, llongnatural.properties,
		llongnatural.nmeths, llongnatural.methods,
		llongnatural.nprivs, llongnatural.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;

     number = cwp_argument(unsigned long long int);
     
     r->primitives[0].ulonglong_value = number;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(float_create) {
     /* Method to create a `cwpFloat' object.
      * Return the new object as a cwp_object_t or `cwpEmpty' on error.
      * Require a number as float as additional argument.
      */
     
     cwp_object_t r;
     float number;
     
     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     r = oalloc(error,
		floatn.nprops, floatn.properties,
		floatn.nmeths, floatn.methods,
		floatn.nprivs, floatn.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;

     /* The compiler says floats becomes double with va_arg,
	so don't be bothered by this line */
     number = cwp_argument(double);
     
     r->primitives[0].float_value = number;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(double_create) {
     /* Method to create a `cwpDouble' object.
      * Return the new object as a cwp_object_t or `cwpEmpty' on error.
      * Require a number as double as additional argument.
      */
     
     cwp_object_t r;
     double number;
     
     cwp_error(cwpNONE);

     if (returned == NULL) {
	  return;
     }

     r = oalloc(error,
		doublen.nprops, doublen.properties,
		doublen.nmeths, doublen.methods,
		doublen.nprivs, doublen.primitives);

     if (r == NULL) {
	  cwp_return(cwpEmpty, cwp_object_t);
	  return;
     }

     r->methods[1].value = object_destroy;

     number = cwp_argument(double);
     
     r->primitives[0].double_value = number;
     
     cwp_return(r, cwp_object_t);
}

static cwp_defmethod(integer_value) {
     /* Method to return the value of a `cwpInt' object.
      * Return a number as int.
      * Doesn't require additional arguments.
      */
     
     int number;
     
     cwp_error(cwpNONE);
     
     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, int);

	  return;
     }

     number = self->primitives[0].int_value;

     cwp_return(number, int);
}

static cwp_defmethod(natural_value) {
     /* Method to return the value of a `cwpNat' object.
      * Return a number as unsigned int.
      * Doesn't require additional arguments.
      */
     
     unsigned int number;

     cwp_error(cwpNONE);

     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, unsigned int);

	  return;
     }

     number = self->primitives[0].uint_value;

     cwp_return(number, unsigned int);
}

static cwp_defmethod(longinteger_value) {
     /* Method to return the value of a `cwpLongInt' object.
      * Return a number as int.
      * Doesn't require additional arguments.
      */
     
     long int number;
     
     cwp_error(cwpNONE);
     
     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, long int);

	  return;
     }

     number = self->primitives[0].long_value;

     cwp_return(number, long int);
}

static cwp_defmethod(longnatural_value) {
     /* Method to return the value of a `cwpLongNat' object.
      * Return a number as unsigned long int.
      * Doesn't require additional arguments.
      */
     
     unsigned long int number;
     
     cwp_error(cwpNONE);
     
     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, unsigned long int);

	  return;
     }

     number = self->primitives[0].ulong_value;

     cwp_return(number, unsigned long int);
}

static cwp_defmethod(llonginteger_value) {
     /* Method to return the value of a `cwpLongLongInt' object.
      * Return a number as long long int.
      * Doesn't require additional arguments.
      */
     
     long long int number;
     
     cwp_error(cwpNONE);
     
     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, long long int);

	  return;
     }

     number = self->primitives[0].longlong_value;

     cwp_return(number, long long int);
}

static cwp_defmethod(llongnatural_value) {
     /* Method to return the value of a `cwpLongLongNat' object.
      * Return a number as unsingned long long int.
      * Doesn't require additional arguments.
      */
     
     unsigned long long int number;
     
     cwp_error(cwpNONE);
     
     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, unsigned long long int);

	  return;
     }

     number = self->primitives[0].ulonglong_value;

     cwp_return(number, unsigned long long int);
}

static cwp_defmethod(float_value) {
     /* Method to return the value of a `cwpFloat' object.
      * Return a number as float.
      * Doesn't require additional arguments.
      */
     
     float number;
     
     cwp_error(cwpNONE);
     
     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, float);

	  return;
     }

     number = self->primitives[0].float_value;

     cwp_return(number, float);
}

static cwp_defmethod(double_value) {
     /* Method to return the value of a `cwpDouble' object.
      * Return a number as double.
      * Doesn't require additional arguments.
      */
     
     double number;
     
     cwp_error(cwpNONE);
     
     if (self == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, double);

	  return;
     }

     number = self->primitives[0].double_value;

     cwp_return(number, double);
}

static cwp_defmethod(number_equals) {
     /* Method to compare two boxed number objects.
      * Return 0 or 1 as int.
      * 0 means the object are different, 1 means they are equal.
      * Requires another object to compare to as additional argument.
      */
     
     cwp_object_t b;
     
     cwp_error(cwpNONE);

     /* Get the other object */
     b = cwp_argument(cwp_object_t);

     if (self == NULL || b == NULL) {
	  cwp_error(cwpARG);
	  cwp_return(0, int);

	  return;
     }

     /* The other object wasn't a number */
     if (self->nprivs != b->nprivs) {
	  cwp_return(0, int);

	  return;
     }

     /* This giant if statement can probably be optimized, but
	just in case every possible case was considered and examined
	It might seems daunting but it's just a comparision between each
	possible primitive field */
     if ((self->primitives[0].int_value != b->primitives[0].int_value
	  && self->primitives[0].int_value != b->primitives[0].uint_value
	  && self->primitives[0].int_value != b->primitives[0].long_value
	  && self->primitives[0].int_value != b->primitives[0].ulong_value
	  && self->primitives[0].int_value != b->primitives[0].longlong_value
	  && self->primitives[0].int_value != b->primitives[0].ulonglong_value
	  && self->primitives[0].int_value != b->primitives[0].float_value
	  && self->primitives[0].int_value != b->primitives[0].double_value)
	 ||
	 (self->primitives[0].uint_value != b->primitives[0].int_value
	  && self->primitives[0].uint_value != b->primitives[0].uint_value
	  && self->primitives[0].uint_value != b->primitives[0].long_value
	  && self->primitives[0].uint_value != b->primitives[0].ulong_value
	  && self->primitives[0].uint_value != b->primitives[0].longlong_value
	  && self->primitives[0].uint_value != b->primitives[0].ulonglong_value
	  && self->primitives[0].uint_value != b->primitives[0].float_value
	  && self->primitives[0].uint_value != b->primitives[0].double_value)
	 ||
	 (self->primitives[0].long_value != b->primitives[0].int_value
	  && self->primitives[0].long_value != b->primitives[0].uint_value
	  && self->primitives[0].long_value != b->primitives[0].long_value
	  && self->primitives[0].long_value != b->primitives[0].ulong_value
	  && self->primitives[0].long_value != b->primitives[0].longlong_value
	  && self->primitives[0].long_value != b->primitives[0].ulonglong_value
	  && self->primitives[0].long_value != b->primitives[0].float_value
	  && self->primitives[0].long_value != b->primitives[0].double_value)
	 ||
	 (self->primitives[0].ulong_value != b->primitives[0].int_value
	  && self->primitives[0].ulong_value != b->primitives[0].uint_value
	  && self->primitives[0].ulong_value != b->primitives[0].long_value
	  && self->primitives[0].ulong_value != b->primitives[0].ulong_value
	  && self->primitives[0].ulong_value != b->primitives[0].longlong_value
	  && self->primitives[0].ulong_value != b->primitives[0].ulonglong_value
	  && self->primitives[0].ulong_value != b->primitives[0].float_value
	  && self->primitives[0].ulong_value != b->primitives[0].double_value)
	 ||
	 (self->primitives[0].longlong_value != b->primitives[0].int_value
	  && self->primitives[0].longlong_value != b->primitives[0].uint_value
	  && self->primitives[0].longlong_value != b->primitives[0].long_value
	  && self->primitives[0].longlong_value != b->primitives[0].ulong_value
	  && self->primitives[0].longlong_value != b->primitives[0].longlong_value
	  && self->primitives[0].longlong_value != b->primitives[0].ulonglong_value
	  && self->primitives[0].longlong_value != b->primitives[0].float_value
	  && self->primitives[0].longlong_value != b->primitives[0].double_value)
	 ||
	 (self->primitives[0].ulonglong_value != b->primitives[0].int_value
	  && self->primitives[0].ulonglong_value != b->primitives[0].uint_value
	  && self->primitives[0].ulonglong_value != b->primitives[0].long_value
	  && self->primitives[0].ulonglong_value != b->primitives[0].ulong_value
	  && self->primitives[0].ulonglong_value != b->primitives[0].longlong_value
	  && self->primitives[0].ulonglong_value != b->primitives[0].ulonglong_value
	  && self->primitives[0].ulonglong_value != b->primitives[0].float_value
	  && self->primitives[0].ulonglong_value != b->primitives[0].double_value)
	 ||
	 (self->primitives[0].float_value != b->primitives[0].int_value
	  && self->primitives[0].float_value != b->primitives[0].uint_value
	  && self->primitives[0].float_value != b->primitives[0].long_value
	  && self->primitives[0].float_value != b->primitives[0].ulong_value
	  && self->primitives[0].float_value != b->primitives[0].longlong_value
	  && self->primitives[0].float_value != b->primitives[0].ulonglong_value
	  && self->primitives[0].float_value != b->primitives[0].float_value
	  && self->primitives[0].float_value != b->primitives[0].double_value)
	 ||
	 (self->primitives[0].double_value != b->primitives[0].int_value
	  && self->primitives[0].double_value != b->primitives[0].uint_value
	  && self->primitives[0].double_value != b->primitives[0].long_value
	  && self->primitives[0].double_value != b->primitives[0].ulong_value
	  && self->primitives[0].double_value != b->primitives[0].longlong_value
	  && self->primitives[0].double_value != b->primitives[0].ulonglong_value
	  && self->primitives[0].double_value != b->primitives[0].float_value
	  && self->primitives[0].double_value != b->primitives[0].double_value)) {
	  /* Everything was different, the objects are not equal */
	  cwp_return(0, int);
     } else {
	  cwp_return(1, int);
     }
}
